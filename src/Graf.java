/*

Přepište úlohu o grafu tak, aby výsledný graf byl
vygenerován jako SVG soubor, kde jednotlivé sloupce budou barevné
obdelníky.

SVG obrázky lze zobrazit např. ve Firefoxu.

Poznámka: cílem úkolu je vyzkoušet si generování jednoduchého SVG.
K tomu stačí základní znalosti SVG, které lze získat koukáním do jeho
zdrojového kódu.
Jde o to, že i když zatím neumíme vytvářet grafické aplikace, můžeme
vytvářet grafické výstupy ;-).

**P.S.:**
Jednoduchý graf může vypadat třeba takhle:

<svg xmlns="http://www.w3.org/2000/svg"
	width="140" height="120"
	viewBox="0 0 140 120">
	<rect x="10" y="10" width="20" height="100"
		fill="blue" stroke="black" stroke-width="2"/>
	<rect x="35" y="50" width="20" height="60"
		fill="blue" stroke="black" stroke-width="2"/>
	<rect x="60" y="30" width="20" height="80"
		fill="blue" stroke="black" stroke-width="2"/>
	<rect x="85" y="90" width="20" height="20"
		fill="blue" stroke="black" stroke-width="2"/>
	<rect x="110" y="70" width="20" height="40"
		fill="blue" stroke="black" stroke-width="2"/>
</svg>

*/
public class Graf {
    public static void main(String args[]) {
    }
}
