/*
Napište program, který vygeneruje 10 náhodných příkladů na sčítání do 20.

Každý příklad vytiskněte na samostatný řádek.

Pro vytváření příkladů se vám bude hodit třída java.util.Random
a její metoda nextInt():


public static void main(String args[]) {
	// 0 v konstruktoru zajistí, že každé spuštění programu se
	// bude chovat stejně.
	// (To se docela hodí pro testování, pro ostrý běh nulu smažte.
	java.util.Random nahoda = new java.util.Random(0);
	int a = nahoda.nextInt(10);
	int b = nahoda.nextInt(5);
	System.out.printf("Nahodne cislo mezi 0 a 9 je %d, mezi 0 a 4 je %d.\n", a, b);
}

*/
public class Scitani {
    public static void main(String args[]) {
    }
}
